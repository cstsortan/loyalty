"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp(functions.config().firebase);
const profsRef = admin.firestore().collection('user-profiles');
// // Start writing Firebase Functions
// // https://firebase.google.com/functions/write-firebase-functions
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
exports.newUserProifle = functions.auth.user()
    .onCreate(event => {
    const user = event.data;
    console.log(user);
    console.log(event);
    const userProfile = {
        name: user.displayName || '',
        phone: user.providerData[0].phoneNumber || '',
        profilePic: user.providerData[0].photoURL || '',
        uid: user.uid,
        email: user.email || '',
    };
    const profDoc = profsRef.doc(user.uid);
    return profDoc.set(userProfile).then(doc => {
        console.log('New document added for user ' + user.uid);
    });
});
//# sourceMappingURL=index.js.map