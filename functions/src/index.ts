import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { UserProfile } from '../../src/app/models/user-profile';

admin.initializeApp(functions.config().firebase);
const profsRef = admin.firestore().collection('user-profiles');

// // Start writing Firebase Functions
// // https://firebase.google.com/functions/write-firebase-functions
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

export const newUserProifle = functions.auth.user()
.onCreate(event => {
  const user = event.data;
  console.log(user);
  console.log(event);
  const userProfile: UserProfile = {
    name: user.displayName || '',
    phone: user.providerData[0].phoneNumber || '',
    profilePic: user.providerData[0].photoURL || '',
    uid: user.uid,
    email: user.email || '',
  };

  const profDoc = profsRef.doc(user.uid);
  return profDoc.update(userProfile).then(doc => {
    console.log('New document added for user ' + user.uid);
  });

});
