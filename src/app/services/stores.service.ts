import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Store, Location } from '../models/store';
import { StoreCard } from '../models/store-card';
import { User } from 'firebase';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/combineLatest';

export const STORES = 'stores';
export const STORE_CARDS = 'store-cards';
export const CARD_TYPES = 'card-types';

@Injectable()
export class StoresService {

  storesCollection: AngularFirestoreCollection<Store>;
  stores: Observable<Store[]>;

  constructor(
    private ds: DataService,
    private afs: AngularFirestore
  ) {
    this.storesCollection = afs.collection<Store>(STORES); // Magic string alert!!!!
    this.stores = this.storesCollection.valueChanges();
  }

  // I think it's clear enough
  getAllStores(): Observable<Store[]> {
    return this.stores;
  }

  // Gives the particular user's stores
  // TODO make sure if it's needed
  getStoresOf(uid: string): Observable<Store[]> {
    return this.afs.collection<Store>(this.storesCollection.ref.path, ref => {
      return ref.where('ownerUid', '==', uid);  // Be careful with this 'ownerUid' magic string!!!!
    }).valueChanges();
  }

  getMyStores(): Observable<Store[]> {
    return this.ds.user$.switchMap((user: User) => {
      return this.getStoresOf(user.uid);
    });
  }

  // I don't know why we need this
  getStore(storeId: string): Observable<Store> {
    return this.storesCollection.doc<Store>(storeId).valueChanges();
  }

  // Creates a new store given a name and a Location object
  createStore(name: string, location: Location): Promise<void> {
    // const id = this.afs.createId();
    const newStore: Store = {
      location: location,
      storeName: name,
      ownerUid: this.afs.app.auth().currentUser.uid, // TODO Find a better way
      // storeId: id,
    };

    // Store the id of this store
    return this.storesCollection
    .add(newStore)
    .then(doc => {
      return doc.update({
        storeId: doc.id,
      });
    });
  }

}
