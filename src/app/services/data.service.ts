import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { User } from 'firebase/app';
import { UserProfile } from '../models/user-profile';

@Injectable()
export class DataService {

  usersCollection: AngularFirestoreCollection<UserProfile>;

  user$: Observable<User>;
  userProfile: Observable<UserProfile>;
  users: Observable<UserProfile[]>;

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
  ) {
    this.user$ = this.afAuth.authState;
    this.usersCollection = this.afs.collection<UserProfile>('user-profiles');
    this.users = this.usersCollection.valueChanges();
    this.userProfile = this.user$.switchMap((user: User) => {
      if (user) {
        // console.log('firebase user exists');
        return this.getUserProfile(user.uid);
      } else {
        // console.log('firebase user should be null and getCurrentUserProfile() sould give a null to the observables');
        return Observable.of(null);
      }
    });
  }

  getCurrentUserProfile(): Observable<UserProfile> {
    return this.userProfile;
  }

  getUserProfile(id: string): Observable<UserProfile|null> {
    return this.usersCollection.doc<UserProfile>(id).valueChanges();
  }

  getUserProfilesList(): Observable<UserProfile[]> {
    return this.usersCollection.valueChanges();
  }

  createUserProfileIfNew(user: User): void {
    this.afs.app.firestore()
    .runTransaction((t => {
      return t.get(this.usersCollection.doc(user.uid).ref)
      .then(doc => {
        if (doc.exists) {
          return t;
        } else {
          const newUserProfile: UserProfile = {
            uid: user.uid,
            name: user.displayName,
            profilePic: user.photoURL || '',
            phone: user.phoneNumber || '',
            email: user.email || '',
          };
          return t.set(doc.ref, newUserProfile);
        }
      });
    }))
    .then(() => {
      console.log('Transaction done successfully');
    })
    .catch(error => {
      console.log('Transaction failed: ' + error);
    });
  }

}
