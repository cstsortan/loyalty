import { Component, OnInit } from '@angular/core';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/subscription';
import * as firebase from 'firebase/app';
import { UserProfile } from './models/user-profile';
import { AngularFireAuth } from 'angularfire2/auth';
import { DataService } from './services/data.service';
import { NbThemeService, NbSidebarService } from '@nebular/theme';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { NewStoreFormComponent } from './components/new-store/new-store-modal-form.component';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-scaffold',
  template: `
  <nb-layout center>
    <nb-layout-header fixed>
      <div class="logo-containter">
        <div class="row">
          <div class="col-sm-auto">
            <a (click)="toggleSidebar()" class="navigation">
              <i class="nb-menu icon"></i>
            </a>
          </div>
          <div class="col"></div>
          <div class="col-sm-auto">
            <button
            mat-raised-button
            color="primary"
            (click)="openStoreCreation()"
            >New Storey</button>
          </div>
        </div>
      </div>
    </nb-layout-header>
  <nb-sidebar left responsive>
    <nb-sidebar-header></nb-sidebar-header>
  </nb-sidebar>
  <nb-layout-column>
    <router-outlet></router-outlet>
  </nb-layout-column>
</nb-layout>
  `,
  styles: [`
    .icon {
      color: #4b0082;
      font-size: 50px;
    }
  `],
})
export class ScaffoldComponent implements OnInit, OnDestroy {
  user$: Observable<firebase.User>;
  userProfile: Observable<UserProfile>;
  userSubscription: Subscription;

  constructor(
    public afAuth: AngularFireAuth,
    private sb: NbSidebarService,
    private router: Router,
    private modalService: NgbModal,
  ) {
    this.user$ = this.afAuth.authState;
  }

  logout(): void {
    this.afAuth.auth.signOut().then(() => {
      this.router.navigate(['auth/login']);
    });
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }

  toggleSidebar(): void {
    this.sb.toggle(false);
  }

  ngOnInit(): void {
    this.userSubscription = this.user$.subscribe(user => {
      // if needed, do a redirect in here
      if (user) {
        console.log('Logged in :)');
        console.log(user);
        // this.ds.createUserProfileIfNew(user);
      } else {
        console.log('Logged out :(');
      }
    });
  }

  openStoreCreation(): void {
    this.modalService.open(NewStoreFormComponent, {
      size: 'lg',
      backdrop: 'static',
      container: 'nb-layout',
    });
  }
}
