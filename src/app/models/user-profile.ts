export interface UserProfile {
  name: string;
  profilePic: string;
  uid: string;
  phone: string;
  email: string;
}
