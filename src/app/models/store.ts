export interface Store {
  storeId?: string;
  storeName: string;
  location: Location;
  ownerUid: string;
}

export interface Location {
  latitude: string;
  longitude: string;
}
