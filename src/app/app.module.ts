// About Angular stuff
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ServiceWorkerModule } from '@angular/service-worker';

// About Nebular
import {
  NbThemeModule,
  NbLayoutModule,
  NbSidebarModule,
  NbSidebarService,
  NbThemeService,
  NbMenuModule,
  NbCardModule,
} from '@nebular/theme';

// Nebular auth module components exists localy in the project
import { NbAuthModule } from './auth/auth.module';
import {
  FirebaseAuthProvider
} from './auth/providers/firebase-auth.provider';

// About Firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from '../environments/environment';

// Project modules
import { AppRoutingModule } from './app-routing.module';

// Components
import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { ScaffoldComponent } from './app.scaffold.component';
import { StoresListItemComponent } from './components/stores-list-item.component';
import { NewStoreFormComponent } from './components/new-store/new-store-modal-form.component';

// Services
import { DataService } from './services/data.service';
import { StoresService } from './services/stores.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { MaterialModule } from './material/material.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalStack } from '@ng-bootstrap/ng-bootstrap/modal/modal-stack';
import { FormsModule } from '@angular/forms';
import { AngularFileDropzoneModule } from 'angular-file-dropzone';


const firebaseModules = [
  AngularFireModule.initializeApp(environment.firebaseConfig),
  AngularFireAuthModule,
  AngularFirestoreModule.enablePersistence(),
];

const services = [
  DataService,
  StoresService,
];

const components = [
  AppComponent,
  HomePageComponent,
  ScaffoldComponent,
  StoresListItemComponent,
  NewStoreFormComponent,
];

const nebularModules = [
  NbThemeModule.forRoot({ name: 'default' }),
    NbLayoutModule,
    NbSidebarModule,
    NbAuthModule.forRoot({
      providers: {
        email: {
          service: FirebaseAuthProvider,
          config: {},
        }
      },
      forms: {},
    }),

    NbCardModule,
];


@NgModule({
  declarations: [
    ...components,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    AngularFileDropzoneModule,
    MaterialModule,
    NgbModule.forRoot(),
    AppRoutingModule,
    ...firebaseModules,
    ...nebularModules,
    ServiceWorkerModule.register('/ngsw-worker.js', {enabled: environment.production}),
  ],
  providers: [
    ...services,
    NbSidebarService,
    NbThemeService
  ],
  entryComponents: [
    NewStoreFormComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
