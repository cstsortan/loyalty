import { Component, OnInit, Input } from '@angular/core';
import { Store } from '../models/store';

@Component({
  selector: 'app-stores-list-item',
  template: `
    <nb-card>
    </nb-card>
  `,
  styles: [`
  `]
})
export class StoresListItemComponent implements OnInit {

  @Input() store: Store;

  constructor() { }

  ngOnInit() {
  }
}
