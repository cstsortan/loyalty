import {Component, OnInit} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DroppedFile } from 'angular-file-dropzone';

@Component({selector: 'app-new-store-form',
  templateUrl: 'new-store-modal-form.component.html',
  styleUrls: ['new-store-modal.component.scss'],
})
export class NewStoreFormComponent implements OnInit {

  files: DroppedFile[] = [];

  constructor(
    private modal: NgbActiveModal,
  ) {}

  ngOnInit() {}

  closeModal(): void {
    this.modal.close();
  }

  fileDroped($event): void {
    const newFile: DroppedFile = $event;
    if (!newFile.type.startsWith('image') || this.files.length > 5) {
      console.log('Not an image');
      return;
    }
    this.files.push($event);
    console.log($event);
  }

  imgClicked(file: DroppedFile) {
    console.log(file.name);
    const i = this.files.indexOf(file);
    if (i > -1) {
      this.files.splice(i, 1);
    }
  }
}
