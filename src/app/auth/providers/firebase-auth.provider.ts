import { Injectable } from '@angular/core';
import { NbAbstractAuthProvider } from './abstract-auth.provider';
import { NbAuthResult } from '../services/auth.service';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from 'firebase/app';

@Injectable()
export class FirebaseAuthProvider extends NbAbstractAuthProvider {

  constructor(
    protected afAuth: AngularFireAuth,
  ) {
    super();
  }

  authenticate(data?: any): Observable<NbAuthResult> {
    return Observable.fromPromise(
      this.afAuth.auth.signInWithEmailAndPassword(data.email, data.password)
      .then((user: User) => {
        return new NbAuthResult(true, null, '/', null, ['Successfully logged in!']);
      })
      .catch(error => {
        const errors: string[] = [];

        switch (error.code) {
          case 'auth/invalid-email':
          errors.push('Invalid Email');
          break;
          case 'auth/user-disabled':
          errors.push('ACcount disabled');
          break;
          case 'auth/user-not-found':
          errors.push('Account not found!');
          break;
          case 'auth/wrong-password':
          errors.push('Wrong password!');
          break;
          default:
            errors.push('Unknown error!');
        }

        return new NbAuthResult(false, null, null, errors);
      })
    );
  }

  register(data?: any): Observable<NbAuthResult> {
    return Observable.fromPromise(
      this.afAuth.auth.createUserWithEmailAndPassword(data.email, data.password)
      .catch(error => {
        const errors: string[] = [];
        if (error.code === 'auth/email-already-in-use') {
          errors.push('Email already in use!');
        } else if (error.code === 'auth/invalid-email') {
          errors.push('Invalid Email!');
        } else if (error.code === 'auth/operation-not-allowed') {
          errors.push('Operation not allowed, Enable email/password accounts in the Firebase Console, under the Auth tab!');
        } else if (error.code === 'auth/weak-password') {
          errors.push('Weak password!');
        } else {
          errors.push('Unknown error!');
        }
        return new NbAuthResult(false, null, '/', errors);
      })
      .then((user: User) => {
        return user.updateProfile({displayName: data.fullName, photoURL: ''}).then(() => user);
      }).then((user: User) => {
        return new NbAuthResult(user != null, null, '/', null, ['Successfully Signed up!']);
      })
    );
  }
  requestPassword(data?: any): Observable<NbAuthResult> {
    return Observable.fromPromise(
      this.afAuth.auth.sendPasswordResetEmail(data.email).then(() => {
        // TODO The third is redirect
        return new NbAuthResult(true, null, null, null, ['Password reset email sent.']);
      }).catch(error => {
        const errors: string[] = [];

        switch (error.code) {
          case 'auth/invalid-email':
          errors.push('Invalid Email!');
          break;
          case 'auth/missing-continue-uri':
          errors.push('Missing continue URL!');
          break;
          case 'auth/user-not-found':
          errors.push('Account not found!');
          break;
          default:
          errors.push('Unknown error!');
          break;
        }
        // TODO redirect if needed is the third
        return new NbAuthResult(false, null, null, errors, errors);
      })
    );
  }
  resetPassword(data?: any): Observable<NbAuthResult> {
    throw new Error('Method not implemented.');
  }
  logout(): Observable<NbAuthResult> {
    return Observable.fromPromise(
      this.afAuth.auth.signOut().then(() => {
        return new NbAuthResult(true);
      })
    );
  }
}
