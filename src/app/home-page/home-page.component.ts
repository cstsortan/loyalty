import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import { UserProfile } from '../models/user-profile';
import { StoresService } from '../services/stores.service';
import { StoreCard } from '../models/store-card';
import { NbThemeService, NbMediaBreakpoint, NbSpinnerService } from '@nebular/theme';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  userProfile: Observable<UserProfile>;
  breakpoint: Observable<NbMediaBreakpoint[]>;

  constructor(
    public ds: DataService,
    public ss: StoresService,
    private afAuth: AngularFireAuth,
    public ts: NbThemeService,
    private spinner: NbSpinnerService,
  ) {
    this.userProfile = this.ds.getCurrentUserProfile();
    this.breakpoint = ts.onMediaQueryChange();
  }

  ngOnInit() {

  }

}
