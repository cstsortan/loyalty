// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBI65FCJ23rHhzLXZKHmEEETkUQ3HTdfaI",
    authDomain: "web-test-2e14e.firebaseapp.com",
    databaseURL: "https://web-test-2e14e.firebaseio.com",
    projectId: "web-test-2e14e",
    storageBucket: "web-test-2e14e.appspot.com",
    messagingSenderId: "99743855001"
  },  
};
