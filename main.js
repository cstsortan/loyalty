const { app, BrowserWindow } = require('electron');

let win;

function createWindow() {
  win = new BrowserWindow({
    width: 1200,
    height: 800,
    backgroundColor: '#ffffff',
    icon: `file://${__dirname}/dist/assets/logo.png`
  });

  win.loadURL(`file://${__dirname}/dist/index.html`)

  // win.webContents.openDevTools();

  win.on('closed', () => {
    win = null;
  });

}

app.isReady('ready', createWindow);

app.on('window-all-closed', () => {
  if(process.platform !== 'darwin') {
    app.quit();
  }
})
